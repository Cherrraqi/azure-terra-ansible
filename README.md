## test webhook
## Description du projet 

Ce projet consiste à créer un cluster de machines identiques attachées à un load blancer sur le cloud Azure. L 'outil utilsé pour créer l 'infrastructure est terraform, après on vise à configurer le cluster des machines virtuelles avec ansible. L'objectif final est d 'orchestrer terraform et ansible avec jenkins.



## Objectif du projet

* Automatisation du provisionnement  et la configuration d'infrastructure sur Azure.
* L'orchestration avec Jenkins.




##### Pourquoi  choisir terraform aux  autres outils IAC ?

* Configuration Vs orchestration
* Infrastructure Mutable vs Infrastructure Immutable
* Procédural vs Déclaratif

Terraform permet de décrire, dans une syntaxe unique, basée sur un formalisme JSON légèrement simplifié pour une meilleure lisibilité, de l’infrastructure à provisionner chez différents fournisseurs. Terraform permet de provisionner une  infrastructure immutable.
Le code Terraform est déclaratif.

###### Pourquoi  choisir Ansible aux  autres outils de configuration ?
* Ansible s 'apprend vite (syntaxe YML)
* Vous pouvez commencer à être productif en quelques heures.Ansible ne necessite pas d 'agent. 
* Ansible est basé sur la connexion ssh pour configurer les serverus distants.





## Provisionnement de l'infrastructure sur azure avec terraform.


### Les  Variables 

On commence par la définition du ficher des variables. Dans cette section on va définir les variables qui vont être utilisé pour configurer les ressources créées par terraform:

```terraform

 variable "storage-account-name" {
  default = "vstsbuildterraform"
}

variable "container-name" {
  default = "terraform-state"
}

variable "rg_prefix" {
  description = "The shortened abbreviation to represent your resource group that will go on the front of some resources."
  default     = "rg"
}

variable "dns_name" {
  description = " Label for the Domain Name. Will be used to make up the FQDN."
  default     = "demojavaiac19"
}

variable "lb_ip_dns_name" {
  description = "DNS for Load Balancer IP"
  default     = "demojavaiac"
}

variable "location" {
  description = "The location/region where the virtual network is created. Changing this forces a new resource to be created."
  default     = "westeurope"
}

variable "virtual_network_name" {
  description = "The name for the virtual network."
  default     = "vnet"
}

variable "address_space" {
  description = "The address space that is used by the virtual network. You can supply more than one address space. Changing this forces a new resource to be created."
  default     = "10.0.0.0/16"
}

variable "subnet_prefix" {
  description = "The address prefix to use for the subnet."
  default     = "10.0.10.0/24"
}

variable "storage_account_tier" {
  description = "Defines the Tier of storage account to be created. Valid options are Standard and Premium."
  default     = "Standard"
}

variable "storage_replication_type" {
  description = "Defines the Replication Type to use for this storage account. Valid options include LRS, GRS etc."
  default     = "LRS"
}

variable "vm_size" {
  description = "Specifies the size of the virtual machine."
  default     = "Standard_D1"
}


```

### Le main


#####  Resource Group
Création d’un groupe de ressources dans Azure

```terraform
resource "azurerm_resource_group" "rg" {
  name     = "demo"
  location = "westeurope"

  tags {
    environment = "Terraform Demo"
  }
}



```
création d’un compte de stockage
```terraform
resource "azurerm_storage_account" "stor" {
  name                     = "${var.dns_name}stor"
  location                 = "${azurerm_resource_group.rg.location}"
  resource_group_name      = "${azurerm_resource_group.rg.name}"
  account_tier             = "${var.storage_account_tier}"
  account_replication_type = "${var.storage_replication_type}"
}


```

##### Availability set in azure 

```terraform

resource "azurerm_availability_set" "avset" {
  name                         = "${var.dns_name}avset"
  location                     = "${azurerm_resource_group.rg.location}"
  resource_group_name          = "${azurerm_resource_group.rg.name}"
  platform_fault_domain_count  = 3
  platform_update_domain_count = 3
  managed                      = true
}



```

Définition de l' adresse IP du load Blancer 
```terraform
resource "azurerm_public_ip" "lbpip" {
  name                         = "${var.lb_ip_dns_name}-ip"
  location                     = "${azurerm_resource_group.rg.location}"
  resource_group_name          = "${azurerm_resource_group.rg.name}"
  public_ip_address_allocation = "dynamic"
  domain_name_label            = "${var.lb_ip_dns_name}"
}


```
##### Infrastructure Network 

Dans cette section on a créé dans un nouveau Azure resource group:

*  Un seul réseau virtuel avec un espace d 'adressage de 10.0.0.0/16
*  Un seul sous réseau avec l 'espace d'adressage 10.0.10.0/24



```terraform
resource "azurerm_virtual_network" "vnet" {
  name                = "${var.virtual_network_name}"
  location            = "${azurerm_resource_group.rg.location}"
  address_space       = ["${var.address_space}"]
  resource_group_name = "${azurerm_resource_group.rg.name}"
}
```
```terraform
resource "azurerm_subnet" "subnet" {
  name                 = "${var.rg_prefix}subnet"
  virtual_network_name = "${azurerm_virtual_network.vnet.name}"
  resource_group_name  = "${azurerm_resource_group.rg.name}"
  address_prefix       = "${var.subnet_prefix}"
}
```
##### Dans cette section on va créer les ressources suivantes:
*  Azure load Balancer attaché à une adresse IP public.
*  Configuration des régules pour le load Blancer.
*  Load Balancer probe configurer dans le load balancer.
*  Les trois machines virtuelles ont le meme vm_dns = http://demojavaiac.westeurope.cloudapp.azure.com
*  les VMS ecoutent sur les ports 50001 50002 50003

```terraform
resource "azurerm_lb" "lb" {
  resource_group_name = "${azurerm_resource_group.rg.name}"
  name                = "${var.rg_prefix}lb"
  location            = "${azurerm_resource_group.rg.location}"

  frontend_ip_configuration {
    name                 = "LoadBalancerFrontEnd"
    public_ip_address_id = "${azurerm_public_ip.lbpip.id}"
  }
}
```

```terraform
resource "azurerm_lb_backend_address_pool" "backend_pool" {
  resource_group_name = "${azurerm_resource_group.rg.name}"
  loadbalancer_id     = "${azurerm_lb.lb.id}"
  name                = "BackendPool1"
}

```


```terraform
resource "azurerm_lb_nat_rule" "tcp" {
  resource_group_name            = "${azurerm_resource_group.rg.name}"
  loadbalancer_id                = "${azurerm_lb.lb.id}"
  name                           = "SSH-VM-${count.index}"
  protocol                       = "tcp"
  frontend_port                  = "5000${count.index + 1}"
  backend_port                   = 22
  frontend_ip_configuration_name = "LoadBalancerFrontEnd"
  count                          = 3
}

```

```terraform
resource "azurerm_lb_rule" "lb_rule" {
  resource_group_name            = "${azurerm_resource_group.rg.name}"
  loadbalancer_id                = "${azurerm_lb.lb.id}"
  name                           = "LBRule"
  protocol                       = "tcp"
  frontend_port                  = 80
  backend_port                   = 8080
  frontend_ip_configuration_name = "LoadBalancerFrontEnd"
  enable_floating_ip             = false
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.backend_pool.id}"
  idle_timeout_in_minutes        = 5
  probe_id                       = "${azurerm_lb_probe.lb_probe.id}"
  depends_on                     = ["azurerm_lb_probe.lb_probe"]
}



```
```terraform
resource "azurerm_lb_probe" "lb_probe" {
  resource_group_name = "${azurerm_resource_group.rg.name}"
  loadbalancer_id     = "${azurerm_lb.lb.id}"
  name                = "tcpProbe"
  protocol            = "tcp"
  port                = 8080
  interval_in_seconds = 5
  number_of_probes    = 3
}


```

Interfaces réseau pour l 'infrastructure avec des IP public
```terraform
resource "azurerm_network_interface" "nic" {
  name                = "nic${count.index}"
  location            = "${azurerm_resource_group.rg.location}"
  resource_group_name = "${azurerm_resource_group.rg.name}"
  count               = 3

  ip_configuration {
    name                                    = "ipconfig${count.index}"
    subnet_id                               = "${azurerm_subnet.subnet.id}"
    private_ip_address_allocation           = "Dynamic"
    load_balancer_backend_address_pools_ids = ["${azurerm_lb_backend_address_pool.backend_pool.id}"]
    load_balancer_inbound_nat_rules_ids     = ["${element(azurerm_lb_nat_rule.tcp.*.id, count.index)}"]
  }
}



```

* Déploiment de 3 machines virtuelles attachées au load balancer
* Configurer l 'accès au groupe de machines

```terraform
resource "azurerm_virtual_machine" "vm" {
  name                  = "vm${count.index}"
  location              = "${azurerm_resource_group.rg.location}"
  resource_group_name   = "${azurerm_resource_group.rg.name}"
  availability_set_id   = "${azurerm_availability_set.avset.id}"
  network_interface_ids = ["${element(azurerm_network_interface.nic.*.id, count.index)}"]
  count                 = 3
  vm_size               = "Standard_DS1_v2"

  storage_os_disk {
    name          = "osdisk${count.index}"
    create_option = "FromImage"
  }
  storage_image_reference {
    publisher = "RedHat"
    offer     = "RHEL"
    sku       = "7.3"
    version   = "latest"
  }
  os_profile {
    computer_name  = "myvm"
    admin_username = "azureuser"
    admin_password = "Passwword1234"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path     = "/home/azureuser/.ssh/authorized_keys"
      key_data = "${var.ssh_key}"
    }
  }

  tags {
    environment = "Terraform Demo"
  }
}


```


## Output

On aura un fichier output.tf  qui va décrire la sortie après le déploiement de notre infrastructure:
les sorties sont le FQDN du laod Balancer le Dns des VMs

```terraform
output "vm_ip" {
  value = "${azurerm_public_ip.lbpip.fqdn}"
}

output "vm_dns" {
  value = "http://${azurerm_public_ip.lbpip.fqdn}"
}
```
La sortie du provisionnement


##### Provisionner notre infrastructure

Initialisation du répertoire de travail terraform

![alt text](images/azur.png)

``source init.sh``

```shell

#!/bin/bash
echo "*****************Authentification************************************"

export ARM_CLIENT_ID=$1
export ARM_CLIENT_SECRET=$2
export ARM_SUBSCRIPTION_ID=$3
export ARM_TENANT_ID=$4
echo "*****************terraform init**************************************"

terraform init  -backend-config=backend.tfvars

```


Deployer  l'infrastructure

``source apply.sh``


```shell
#!/bin/bash
echo "************* execute terraform apply"
## execute terrafotm build and sendout to packer-build-output
export ARM_CLIENT_ID=$1
export ARM_CLIENT_SECRET=$2
export ARM_SUBSCRIPTION_ID=$3
export ARM_TENANT_ID=$4
terraform apply  -auto-approve -var ssh_key=$5
export vmss_ip=$(terraform output vm_ip)
echo "host1 ansible_ssh_port=50001 ansible_ssh_host=$vmss_ip" > inventory
echo "host2 ansible_port=50002 ansible_ssh_host=$vmss_ip" >> inventory
echo "host3 ansible_port=50003 ansible_ssh_host=$vmss_ip" >> inventory

cat inventory

```

![alt text](images/output.png)

## Déployer Une Application web avec ansible

L inventaire quand va utiliser et le suivant :

```shell
host1 ansible_ssh_port=50001 ansible_ssh_host=demojavaiac.westeurope.cloudapp.azure.com
host2 ansible_ssh_port=50002 ansible_ssh_host=demojavaiac.westeurope.cloudapp.azure.com
host3 ansible_port=50003 ansible_ssh_host=demojavaiac.westeurope.cloudapp.azure.com
```
``ansible-playbook -i inventory Java-app.yml ``

* Installation et configuration Security-Enhanced Linux `SElinux`
* Installation de Tomcat et déploiment de l 'application sur le group de machines



![alt text](images/app.png.png)

## Jenkins job

#### Integration de terraform avec jenkins
Ajouter le plugin Terraform    ``Manage Jenkins — Install Plugin — Terraform``

Configuration de terraform     ``Manage Jenkins ->Global Tool Configuration``

Configuration des varibales d 'environnemt ``Manage Jenkins -> Configure System -> Set environment variable``


###### Création d 'un pipeline Jenkins


```terraform
node {
     stage('get code') {
      git credentialsId: 'Gitlabcherrraqicredentials', url: 'https://gitlab.com/Cherrraqi/azure-terra-ansible.git'
   }
   stage('Preparation') { 
      env.PATH = "${tfHome}:${env.PATH}"
   stage('terraform init'){
      sh 'cd /root/.jenkins/workspace/test && terraform init'}
   stage('terraform plan'){
      sh 'cd /root/.jenkins/workspace/test && terraform plan -var ssh-key="xxxxxxxxxxxx"'}
   stage('terraform apply'){
      sh 'cd /root/.jenkins/workspace/test && terraform  apply -auto-approve -var ssh-key="xxxxxxxx"'}
   }
   stage('Configure and deploy with ansible') {
         ansiblePlaybook( 
        playbook: '/root/.jenkins/workspace/test/ansible/Java-app.yml',
        inventory: '/root/.jenkins/workspace/test/ansible/inventory', 
        colorized: true)
   }
}
```

![alt text](images/jenkins.png)

#### La sortie de l 'execution du job jenkins
```python
def jenkins-output()
    """voici ma belle documentation sur plusieurs lignes
Running in Durability level: MAX_SURVIVABILITY
[Pipeline] node

Running on Jenkins in /root/.jenkins/workspace/test
[Pipeline] {

[Pipeline] stage

[Pipeline] { (get code)

[Pipeline] git

using credential Gitlabcherrraqicredentials
 > git rev-parse --is-inside-work-tree # timeout=10
Fetching changes from the remote Git repository
 > git config remote.origin.url https://gitlab.com/Cherrraqi/azure-terra-ansible.git # timeout=10
Fetching upstream changes from https://gitlab.com/Cherrraqi/azure-terra-ansible.git
 > git --version # timeout=10
using GIT_ASKPASS to set credentials Gitlabcherrraqicredentials
 > git fetch --tags --progress https://gitlab.com/Cherrraqi/azure-terra-ansible.git +refs/heads/*:refs/remotes/origin/* # timeout=10
 > git rev-parse refs/remotes/origin/master^{commit} # timeout=10
 > git rev-parse refs/remotes/origin/origin/master^{commit} # timeout=10
Checking out Revision xxxxxxxxxxxxxxxxxxxxxxxxxxx (refs/remotes/origin/master)
 > git config core.sparsecheckout # timeout=10
 > git checkout -f xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx # timeout=10
 > git branch -a -v --no-abbrev # timeout=10
 > git branch -D master # timeout=10
 > git checkout -b master xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx # timeout=10
Commit message: "Update inventory"
 > git rev-list --no-walk xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx # timeout=10
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage

[Pipeline] { (Preparation)

[Pipeline] sh

+ cd /root/.jenkins/workspace/test
+ terraform init

[0m[1mInitializing provider plugins...[0m
- Checking for available provider plugins on https://releases.hashicorp.com...
- Downloading plugin for provider "azurerm" (1.24.0)...

The following providers do not have any version constraints in configuration,
so the latest version was installed.

To prevent automatic upgrades to new major versions that may contain breaking
changes, it is recommended to add version = "..." constraints to the
corresponding provider blocks in configuration, with the constraint strings
suggested below.

* provider.azurerm: version = "~> 1.24"

[0m[1m[32mTerraform has been successfully initialized![0m[32m[0m
[0m[32m
You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.[0m
[Pipeline] sh

'''groovy
+ cd /root/.jenkins/workspace/test
+ terraform plan -var 'ssh-key=ssh-rsa xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
[33m
[1m[33mWarning: [0m[0m[1mazurerm_network_interface.nic[0]: "ip_configuration.0.load_balancer_backend_address_pools_ids": [DEPRECATED] This field has been deprecated in favour of the `azurerm_network_interface_backend_address_pool_association` resource.[0m

[0m[0m[0m
[33m
[1m[33mWarning: [0m[0m[1mazurerm_network_interface.nic[0]: "ip_configuration.0.load_balancer_inbound_nat_rules_ids": [DEPRECATED] This field has been deprecated in favour of the `azurerm_network_interface_nat_rule_association` resource.[0m

[0m[0m[0m
[33m
[1m[33mWarning: [0m[0m[1mazurerm_network_interface.nic[1]: "ip_configuration.0.load_balancer_backend_address_pools_ids": [DEPRECATED] This field has been deprecated in favour of the `azurerm_network_interface_backend_address_pool_association` resource.[0m

[0m[0m[0m
[33m
[1m[33mWarning: [0m[0m[1mazurerm_network_interface.nic[1]: "ip_configuration.0.load_balancer_inbound_nat_rules_ids": [DEPRECATED] This field has been deprecated in favour of the `azurerm_network_interface_nat_rule_association` resource.[0m

[0m[0m[0m
[33m
[1m[33mWarning: [0m[0m[1mazurerm_network_interface.nic[2]: "ip_configuration.0.load_balancer_backend_address_pools_ids": [DEPRECATED] This field has been deprecated in favour of the `azurerm_network_interface_backend_address_pool_association` resource.[0m

[0m[0m[0m
[33m
[1m[33mWarning: [0m[0m[1mazurerm_network_interface.nic[2]: "ip_configuration.0.load_balancer_inbound_nat_rules_ids": [DEPRECATED] This field has been deprecated in favour of the `azurerm_network_interface_nat_rule_association` resource.[0m

[0m[0m[0m
[33m
[1m[33mWarning: [0m[0m[1mazurerm_public_ip.lbpip: "public_ip_address_allocation": [DEPRECATED] this property has been deprecated in favor of `allocation_method` to better match the api[0m

[0m[0m[0m

[0m[1mRefreshing Terraform state in-memory prior to plan...[0m
The refreshed state will be used to calculate this plan, but will not be
persisted to local or remote state storage.
[0m

------------------------------------------------------------------------

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  [32m+[0m create
[0m
Terraform will perform the following actions:

[32m  [32m+[0m [32mazurerm_availability_set.avset
[0m      id:                                                                 <computed>
      location:                                                           "westeurope"
      managed:                                                            "true"
      name:                                                               "demojavaiac19avset"
      platform_fault_domain_count:                                        "3"
      platform_update_domain_count:                                       "3"
      resource_group_name:                                                "demo"
      tags.%:                                                             <computed>
[0m
[0m[32m  [32m+[0m [32mazurerm_lb.lb
[0m      id:                                                                 <computed>
      frontend_ip_configuration.#:                                        "1"
      frontend_ip_configuration.0.inbound_nat_rules.#:                    <computed>
      frontend_ip_configuration.0.load_balancer_rules.#:                  <computed>
      frontend_ip_configuration.0.name:                                   "LoadBalancerFrontEnd"
      frontend_ip_configuration.0.outbound_rules.#:                       <computed>
      frontend_ip_configuration.0.private_ip_address:                     <computed>
      frontend_ip_configuration.0.public_ip_address_id:                   "${azurerm_public_ip.lbpip.id}"
      frontend_ip_configuration.0.subnet_id:                              <computed>
      location:                                                           "westeurope"
      name:                                                               "rglb"
      private_ip_address:                                                 <computed>
      private_ip_addresses.#:                                             <computed>
      resource_group_name:                                                "demo"
      sku:                                                                "Basic"
      tags.%:                                                             <computed>
[0m
[0m[32m  [32m+[0m [32mazurerm_lb_backend_address_pool.backend_pool
[0m      id:                                                                 <computed>
      backend_ip_configurations.#:                                        <computed>
      load_balancing_rules.#:                                             <computed>
      loadbalancer_id:                                                    "${azurerm_lb.lb.id}"
      name:                                                               "BackendPool1"
      resource_group_name:                                                "demo"
[0m
[0m[32m  [32m+[0m [32mazurerm_lb_nat_rule.tcp[0]
[0m      id:                                                                 <computed>
      backend_ip_configuration_id:                                        <computed>
      backend_port:                                                       "22"
      enable_floating_ip:                                                 <computed>
      frontend_ip_configuration_id:                                       <computed>
      frontend_ip_configuration_name:                                     "LoadBalancerFrontEnd"
      frontend_port:                                                      "50001"
      loadbalancer_id:                                                    "${azurerm_lb.lb.id}"
      name:                                                               "SSH-VM-0"
      protocol:                                                           "tcp"
      resource_group_name:                                                "demo"
[0m
[0m[32m  [32m+[0m [32mazurerm_lb_nat_rule.tcp[1]
[0m      id:                                                                 <computed>
      backend_ip_configuration_id:                                        <computed>
      backend_port:                                                       "22"
      enable_floating_ip:                                                 <computed>
      frontend_ip_configuration_id:                                       <computed>
      frontend_ip_configuration_name:                                     "LoadBalancerFrontEnd"
      frontend_port:                                                      "50002"
      loadbalancer_id:                                                    "${azurerm_lb.lb.id}"
      name:                                                               "SSH-VM-1"
      protocol:                                                           "tcp"
      resource_group_name:                                                "demo"
[0m
[0m[32m  [32m+[0m [32mazurerm_lb_nat_rule.tcp[2]
[0m      id:                                                                 <computed>
      backend_ip_configuration_id:                                        <computed>
      backend_port:                                                       "22"
      enable_floating_ip:                                                 <computed>
      frontend_ip_configuration_id:                                       <computed>
      frontend_ip_configuration_name:                                     "LoadBalancerFrontEnd"
      frontend_port:                                                      "50003"
      loadbalancer_id:                                                    "${azurerm_lb.lb.id}"
      name:                                                               "SSH-VM-2"
      protocol:                                                           "tcp"
      resource_group_name:                                                "demo"
[0m
[0m[32m  [32m+[0m [32mazurerm_lb_probe.lb_probe
[0m      id:                                                                 <computed>
      interval_in_seconds:                                                "5"
      load_balancer_rules.#:                                              <computed>
      loadbalancer_id:                                                    "${azurerm_lb.lb.id}"
      name:                                                               "tcpProbe"
      number_of_probes:                                                   "3"
      port:                                                               "8080"
      protocol:                                                           "tcp"
      resource_group_name:                                                "demo"
[0m
[0m[32m  [32m+[0m [32mazurerm_lb_rule.lb_rule
[0m      id:                                                                 <computed>
      backend_address_pool_id:                                            "${azurerm_lb_backend_address_pool.backend_pool.id}"
      backend_port:                                                       "8080"
      enable_floating_ip:                                                 "false"
      frontend_ip_configuration_id:                                       <computed>
      frontend_ip_configuration_name:                                     "LoadBalancerFrontEnd"
      frontend_port:                                                      "80"
      idle_timeout_in_minutes:                                            "5"
      load_distribution:                                                  <computed>
      loadbalancer_id:                                                    "${azurerm_lb.lb.id}"
      name:                                                               "LBRule"
      probe_id:                                                           "${azurerm_lb_probe.lb_probe.id}"
      protocol:                                                           "tcp"
      resource_group_name:                                                "demo"
[0m
[0m[32m  [32m+[0m [32mazurerm_network_interface.nic[0]
[0m      id:                                                                 <computed>
      applied_dns_servers.#:                                              <computed>
      dns_servers.#:                                                      <computed>
      enable_accelerated_networking:                                      "false"
      enable_ip_forwarding:                                               "false"
      internal_dns_name_label:                                            <computed>
      internal_fqdn:                                                      <computed>
      ip_configuration.#:                                                 "1"
      ip_configuration.0.application_gateway_backend_address_pools_ids.#: <computed>
      ip_configuration.0.application_security_group_ids.#:                <computed>
      ip_configuration.0.load_balancer_backend_address_pools_ids.#:       <computed>
      ip_configuration.0.load_balancer_inbound_nat_rules_ids.#:           <computed>
      ip_configuration.0.name:                                            "ipconfig0"
      ip_configuration.0.primary:                                         <computed>
      ip_configuration.0.private_ip_address_allocation:                   "dynamic"
      ip_configuration.0.private_ip_address_version:                      "IPv4"
      ip_configuration.0.subnet_id:                                       "${azurerm_subnet.subnet.id}"
      location:                                                           "westeurope"
      mac_address:                                                        <computed>
      name:                                                               "nic0"
      private_ip_address:                                                 <computed>
      private_ip_addresses.#:                                             <computed>
      resource_group_name:                                                "demo"
      tags.%:                                                             <computed>
      virtual_machine_id:                                                 <computed>
[0m
[0m[32m  [32m+[0m [32mazurerm_network_interface.nic[1]
[0m      id:                                                                 <computed>
      applied_dns_servers.#:                                              <computed>
      dns_servers.#:                                                      <computed>
      enable_accelerated_networking:                                      "false"
      enable_ip_forwarding:                                               "false"
      internal_dns_name_label:                                            <computed>
      internal_fqdn:                                                      <computed>
      ip_configuration.#:                                                 "1"
      ip_configuration.0.application_gateway_backend_address_pools_ids.#: <computed>
      ip_configuration.0.application_security_group_ids.#:                <computed>
      ip_configuration.0.load_balancer_backend_address_pools_ids.#:       <computed>
      ip_configuration.0.load_balancer_inbound_nat_rules_ids.#:           <computed>
      ip_configuration.0.name:                                            "ipconfig1"
      ip_configuration.0.primary:                                         <computed>
      ip_configuration.0.private_ip_address_allocation:                   "dynamic"
      ip_configuration.0.private_ip_address_version:                      "IPv4"
      ip_configuration.0.subnet_id:                                       "${azurerm_subnet.subnet.id}"
      location:                                                           "westeurope"
      mac_address:                                                        <computed>
      name:                                                               "nic1"
      private_ip_address:                                                 <computed>
      private_ip_addresses.#:                                             <computed>
      resource_group_name:                                                "demo"
      tags.%:                                                             <computed>
      virtual_machine_id:                                                 <computed>
[0m
[0m[32m  [32m+[0m [32mazurerm_network_interface.nic[2]
[0m      id:                                                                 <computed>
      applied_dns_servers.#:                                              <computed>
      dns_servers.#:                                                      <computed>
      enable_accelerated_networking:                                      "false"
      enable_ip_forwarding:                                               "false"
      internal_dns_name_label:                                            <computed>
      internal_fqdn:                                                      <computed>
      ip_configuration.#:                                                 "1"
      ip_configuration.0.application_gateway_backend_address_pools_ids.#: <computed>
      ip_configuration.0.application_security_group_ids.#:                <computed>
      ip_configuration.0.load_balancer_backend_address_pools_ids.#:       <computed>
      ip_configuration.0.load_balancer_inbound_nat_rules_ids.#:           <computed>
      ip_configuration.0.name:                                            "ipconfig2"
      ip_configuration.0.primary:                                         <computed>
      ip_configuration.0.private_ip_address_allocation:                   "dynamic"
      ip_configuration.0.private_ip_address_version:                      "IPv4"
      ip_configuration.0.subnet_id:                                       "${azurerm_subnet.subnet.id}"
      location:                                                           "westeurope"
      mac_address:                                                        <computed>
      name:                                                               "nic2"
      private_ip_address:                                                 <computed>
      private_ip_addresses.#:                                             <computed>
      resource_group_name:                                                "demo"
      tags.%:                                                             <computed>
      virtual_machine_id:                                                 <computed>
[0m
[0m[32m  [32m+[0m [32mazurerm_public_ip.lbpip
[0m      id:                                                                 <computed>
      allocation_method:                                                  <computed>
      domain_name_label:                                                  "demojavaiac"
      fqdn:                                                               <computed>
      idle_timeout_in_minutes:                                            "4"
      ip_address:                                                         <computed>
      ip_version:                                                         "IPv4"
      location:                                                           "westeurope"
      name:                                                               "demojavaiac-ip"
      public_ip_address_allocation:                                       "dynamic"
      resource_group_name:                                                "demo"
      sku:                                                                "Basic"
      tags.%:                                                             <computed>
[0m
[0m[32m  [32m+[0m [32mazurerm_resource_group.rg
[0m      id:                                                                 <computed>
      location:                                                           "westeurope"
      name:                                                               "demo"
      tags.%:                                                             "1"
      tags.environment:                                                   "Terraform Demo"
[0m
[0m[32m  [32m+[0m [32mazurerm_storage_account.stor
[0m      id:                                                                 <computed>
      access_tier:                                                        <computed>
      account_encryption_source:                                          "Microsoft.Storage"
      account_kind:                                                       "Storage"
      account_replication_type:                                           "LRS"
      account_tier:                                                       "Standard"
      enable_blob_encryption:                                             "true"
      enable_file_encryption:                                             "true"
      identity.#:                                                         <computed>
      is_hns_enabled:                                                     "false"
      location:                                                           "westeurope"
      name:                                                               "demojavaiac19stor"
      primary_access_key:                                                 <computed>
      primary_blob_connection_string:                                     <computed>
      primary_blob_endpoint:                                              <computed>
      primary_blob_host:                                                  <computed>
      primary_connection_string:                                          <computed>
      primary_dfs_endpoint:                                               <computed>
      primary_dfs_host:                                                   <computed>
      primary_file_endpoint:                                              <computed>
      primary_file_host:                                                  <computed>
      primary_location:                                                   <computed>
      primary_queue_endpoint:                                             <computed>
      primary_queue_host:                                                 <computed>
      primary_table_endpoint:                                             <computed>
      primary_table_host:                                                 <computed>
      primary_web_endpoint:                                               <computed>
      primary_web_host:                                                   <computed>
      resource_group_name:                                                "demo"
      secondary_access_key:                                               <computed>
      secondary_blob_connection_string:                                   <computed>
      secondary_blob_endpoint:                                            <computed>
      secondary_blob_host:                                                <computed>
      secondary_connection_string:                                        <computed>
      secondary_dfs_endpoint:                                             <computed>
      secondary_dfs_host:                                                 <computed>
      secondary_file_endpoint:                                            <computed>
      secondary_file_host:                                                <computed>
      secondary_location:                                                 <computed>
      secondary_queue_endpoint:                                           <computed>
      secondary_queue_host:                                               <computed>
      secondary_table_endpoint:                                           <computed>
      secondary_table_host:                                               <computed>
      secondary_web_endpoint:                                             <computed>
      secondary_web_host:                                                 <computed>
      tags.%:                                                             <computed>
[0m
[0m[32m  [32m+[0m [32mazurerm_subnet.subnet
[0m      id:                                                                 <computed>
      address_prefix:                                                     "10.0.10.0/24"
      ip_configurations.#:                                                <computed>
      name:                                                               "rgsubnet"
      resource_group_name:                                                "demo"
      virtual_network_name:                                               "vnet"
[0m
[0m[32m  [32m+[0m [32mazurerm_virtual_machine.vm[0]
[0m      id:                                                                 <computed>
      availability_set_id:                                                "${azurerm_availability_set.avset.id}"
      delete_data_disks_on_termination:                                   "false"
      delete_os_disk_on_termination:                                      "false"
      identity.#:                                                         <computed>
      location:                                                           "westeurope"
      name:                                                               "vm0"
      network_interface_ids.#:                                            <computed>
      os_profile.#:                                                       "1"
      os_profile.1770182618.admin_password:                               <sensitive>
      os_profile.1770182618.admin_username:                               "azureuser"
      os_profile.1770182618.computer_name:                                "myvm"
      os_profile.1770182618.custom_data:                                  <computed>
      os_profile_linux_config.#:                                          "1"
      os_profile_linux_config.69840937.disable_password_authentication:   "true"
      os_profile_linux_config.69840937.ssh_keys.#:                        "1"
      os_profile_linux_config.69840937.ssh_keys.0.key_data:               "ssh-rsa xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
      os_profile_linux_config.69840937.ssh_keys.0.path:                   "/home/azureuser/.ssh/authorized_keys"
      resource_group_name:                                                "demo"
      storage_data_disk.#:                                                <computed>
      storage_image_reference.#:                                          "1"
      storage_image_reference.2250550379.id:                              ""
      storage_image_reference.2250550379.offer:                           "RHEL"
      storage_image_reference.2250550379.publisher:                       "RedHat"
      storage_image_reference.2250550379.sku:                             "7.3"
      storage_image_reference.2250550379.version:                         "latest"
      storage_os_disk.#:                                                  "1"
      storage_os_disk.0.caching:                                          <computed>
      storage_os_disk.0.create_option:                                    "FromImage"
      storage_os_disk.0.disk_size_gb:                                     <computed>
      storage_os_disk.0.managed_disk_id:                                  <computed>
      storage_os_disk.0.managed_disk_type:                                <computed>
      storage_os_disk.0.name:                                             "osdisk0"
      storage_os_disk.0.write_accelerator_enabled:                        "false"
      tags.%:                                                             "1"
      tags.environment:                                                   "Terraform Demo"
      vm_size:                                                            "Standard_DS1_v2"
[0m
[0m[32m  [32m+[0m [32mazurerm_virtual_machine.vm[1]
[0m      id:                                                                 <computed>
      availability_set_id:                                                "${azurerm_availability_set.avset.id}"
      delete_data_disks_on_termination:                                   "false"
      delete_os_disk_on_termination:                                      "false"
      identity.#:                                                         <computed>
      location:                                                           "westeurope"
      name:                                                               "vm1"
      network_interface_ids.#:                                            <computed>
      os_profile.#:                                                       "1"
      os_profile.1770182618.admin_password:                               <sensitive>
      os_profile.1770182618.admin_username:                               "azureuser"
      os_profile.1770182618.computer_name:                                "myvm"
      os_profile.1770182618.custom_data:                                  <computed>
      os_profile_linux_config.#:                                          "1"
      os_profile_linux_config.69840937.disable_password_authentication:   "true"
      os_profile_linux_config.69840937.ssh_keys.#:                        "1"
      os_profile_linux_config.69840937.ssh_keys.0.key_data:               "ssh-rsa xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
      os_profile_linux_config.69840937.ssh_keys.0.path:                   "/home/azureuser/.ssh/authorized_keys"
      resource_group_name:                                                "demo"
      storage_data_disk.#:                                                <computed>
      storage_image_reference.#:                                          "1"
      storage_image_reference.2250550379.id:                              ""
      storage_image_reference.2250550379.offer:                           "RHEL"
      storage_image_reference.2250550379.publisher:                       "RedHat"
      storage_image_reference.2250550379.sku:                             "7.3"
      storage_image_reference.2250550379.version:                         "latest"
      storage_os_disk.#:                                                  "1"
      storage_os_disk.0.caching:                                          <computed>
      storage_os_disk.0.create_option:                                    "FromImage"
      storage_os_disk.0.disk_size_gb:                                     <computed>
      storage_os_disk.0.managed_disk_id:                                  <computed>
      storage_os_disk.0.managed_disk_type:                                <computed>
      storage_os_disk.0.name:                                             "osdisk1"
      storage_os_disk.0.write_accelerator_enabled:                        "false"
      tags.%:                                                             "1"
      tags.environment:                                                   "Terraform Demo"
      vm_size:                                                            "Standard_DS1_v2"
[0m
[0m[32m  [32m+[0m [32mazurerm_virtual_machine.vm[2]
[0m      id:                                                                 <computed>
      availability_set_id:                                                "${azurerm_availability_set.avset.id}"
      delete_data_disks_on_termination:                                   "false"
      delete_os_disk_on_termination:                                      "false"
      identity.#:                                                         <computed>
      location:                                                           "westeurope"
      name:                                                               "vm2"
      network_interface_ids.#:                                            <computed>
      os_profile.#:                                                       "1"
      os_profile.1770182618.admin_password:                               <sensitive>
      os_profile.1770182618.admin_username:                               "azureuser"
      os_profile.1770182618.computer_name:                                "myvm"
      os_profile.1770182618.custom_data:                                  <computed>
      os_profile_linux_config.#:                                          "1"
      os_profile_linux_config.69840937.disable_password_authentication:   "true"
      os_profile_linux_config.69840937.ssh_keys.#:                        "1"
      os_profile_linux_config.69840937.ssh_keys.0.key_data:               "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
      os_profile_linux_config.69840937.ssh_keys.0.path:                   "/home/azureuser/.ssh/authorized_keys"
      resource_group_name:                                                "demo"
      storage_data_disk.#:                                                <computed>
      storage_image_reference.#:                                          "1"
      storage_image_reference.2250550379.id:                              ""
      storage_image_reference.2250550379.offer:                           "RHEL"
      storage_image_reference.2250550379.publisher:                       "RedHat"
      storage_image_reference.2250550379.sku:                             "7.3"
      storage_image_reference.2250550379.version:                         "latest"
      storage_os_disk.#:                                                  "1"
      storage_os_disk.0.caching:                                          <computed>
      storage_os_disk.0.create_option:                                    "FromImage"
      storage_os_disk.0.disk_size_gb:                                     <computed>
      storage_os_disk.0.managed_disk_id:                                  <computed>
      storage_os_disk.0.managed_disk_type:                                <computed>
      storage_os_disk.0.name:                                             "osdisk2"
      storage_os_disk.0.write_accelerator_enabled:                        "false"
      tags.%:                                                             "1"
      tags.environment:                                                   "Terraform Demo"
      vm_size:                                                            "Standard_DS1_v2"
[0m
[0m[32m  [32m+[0m [32mazurerm_virtual_network.vnet
[0m      id:                                                                 <computed>
      address_space.#:                                                    "1"
      address_space.0:                                                    "10.0.0.0/16"
      location:                                                           "westeurope"
      name:                                                               "vnet"
      resource_group_name:                                                "demo"
      subnet.#:                                                           <computed>
      tags.%:                                                             <computed>
[0m
[0m
[0m[1mPlan:[0m 19 to add, 0 to change, 0 to destroy.[0m

------------------------------------------------------------------------

Note: You didn't specify an "-out" parameter to save this plan, so Terraform
can't guarantee that exactly these actions will be performed if
"terraform apply" is subsequently run.

[Pipeline] sh

+ cd /root/.jenkins/workspace/test
+ terraform apply -auto-approve -var 'ssh-key=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
[33m
[1m[33mWarning: [0m[0m[1mazurerm_network_interface.nic[0]: "ip_configuration.0.load_balancer_backend_address_pools_ids": [DEPRECATED] This field has been deprecated in favour of the `azurerm_network_interface_backend_address_pool_association` resource.[0m

[0m[0m[0m
[33m
[1m[33mWarning: [0m[0m[1mazurerm_network_interface.nic[0]: "ip_configuration.0.load_balancer_inbound_nat_rules_ids": [DEPRECATED] This field has been deprecated in favour of the `azurerm_network_interface_nat_rule_association` resource.[0m

[0m[0m[0m
[33m
[1m[33mWarning: [0m[0m[1mazurerm_network_interface.nic[1]: "ip_configuration.0.load_balancer_backend_address_pools_ids": [DEPRECATED] This field has been deprecated in favour of the `azurerm_network_interface_backend_address_pool_association` resource.[0m

[0m[0m[0m
[33m
[1m[33mWarning: [0m[0m[1mazurerm_network_interface.nic[1]: "ip_configuration.0.load_balancer_inbound_nat_rules_ids": [DEPRECATED] This field has been deprecated in favour of the `azurerm_network_interface_nat_rule_association` resource.[0m

[0m[0m[0m
[33m
[1m[33mWarning: [0m[0m[1mazurerm_network_interface.nic[2]: "ip_configuration.0.load_balancer_backend_address_pools_ids": [DEPRECATED] This field has been deprecated in favour of the `azurerm_network_interface_backend_address_pool_association` resource.[0m

[0m[0m[0m
[33m
[1m[33mWarning: [0m[0m[1mazurerm_network_interface.nic[2]: "ip_configuration.0.load_balancer_inbound_nat_rules_ids": [DEPRECATED] This field has been deprecated in favour of the `azurerm_network_interface_nat_rule_association` resource.[0m

[0m[0m[0m
[33m
[1m[33mWarning: [0m[0m[1mazurerm_public_ip.lbpip: "public_ip_address_allocation": [DEPRECATED] this property has been deprecated in favor of `allocation_method` to better match the api[0m

[0m[0m[0m

[0m[1mazurerm_resource_group.rg: Creating...[0m
  location:         "" => "westeurope"
  name:             "" => "demo"
  tags.%:           "" => "1"
  tags.environment: "" => "Terraform Demo"[0m
[0m[1mazurerm_resource_group.rg: Creation complete after 0s (ID: /subscriptions/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx/resourceGroups/demo)[0m[0m
[0m[1mazurerm_availability_set.avset: Creating...[0m
  location:                     "" => "westeurope"
  managed:                      "" => "true"
  name:                         "" => "demojavaiac19avset"
  platform_fault_domain_count:  "" => "3"
  platform_update_domain_count: "" => "3"
  resource_group_name:          "" => "demo"
  tags.%:                       "" => "<computed>"[0m
[0m[1mazurerm_public_ip.lbpip: Creating...[0m
  allocation_method:            "" => "<computed>"
  domain_name_label:            "" => "demojavaiac"
  fqdn:                         "" => "<computed>"
  idle_timeout_in_minutes:      "" => "4"
  ip_address:                   "" => "<computed>"
  ip_version:                   "" => "IPv4"
  location:                     "" => "westeurope"
  name:                         "" => "demojavaiac-ip"
  public_ip_address_allocation: "" => "dynamic"
  resource_group_name:          "" => "demo"
  sku:                          "" => "Basic"
  tags.%:                       "" => "<computed>"[0m
[0m[1mazurerm_storage_account.stor: Creating...[0m
  access_tier:                      "" => "<computed>"
  account_encryption_source:        "" => "Microsoft.Storage"
  account_kind:                     "" => "Storage"
  account_replication_type:         "" => "LRS"
  account_tier:                     "" => "Standard"
  enable_blob_encryption:           "" => "true"
  enable_file_encryption:           "" => "true"
  identity.#:                       "" => "<computed>"
  is_hns_enabled:                   "" => "false"
  location:                         "" => "westeurope"
  name:                             "" => "demojavaiac19stor"
  primary_access_key:               "<sensitive>" => "<sensitive>"
  primary_blob_connection_string:   "<sensitive>" => "<sensitive>"
  primary_blob_endpoint:            "" => "<computed>"
  primary_blob_host:                "" => "<computed>"
  primary_connection_string:        "<sensitive>" => "<sensitive>"
  primary_dfs_endpoint:             "" => "<computed>"
  primary_dfs_host:                 "" => "<computed>"
  primary_file_endpoint:            "" => "<computed>"
  primary_file_host:                "" => "<computed>"
  primary_location:                 "" => "<computed>"
  primary_queue_endpoint:           "" => "<computed>"
  primary_queue_host:               "" => "<computed>"
  primary_table_endpoint:           "" => "<computed>"
  primary_table_host:               "" => "<computed>"
  primary_web_endpoint:             "" => "<computed>"
  primary_web_host:                 "" => "<computed>"
  resource_group_name:              "" => "demo"
  secondary_access_key:             "<sensitive>" => "<sensitive>"
  secondary_blob_connection_string: "<sensitive>" => "<sensitive>"
  secondary_blob_endpoint:          "" => "<computed>"
  secondary_blob_host:              "" => "<computed>"
  secondary_connection_string:      "<sensitive>" => "<sensitive>"
  secondary_dfs_endpoint:           "" => "<computed>"
  secondary_dfs_host:               "" => "<computed>"
  secondary_file_endpoint:          "" => "<computed>"
  secondary_file_host:              "" => "<computed>"
  secondary_location:               "" => "<computed>"
  secondary_queue_endpoint:         "" => "<computed>"
  secondary_queue_host:             "" => "<computed>"
  secondary_table_endpoint:         "" => "<computed>"
  secondary_table_host:             "" => "<computed>"
  secondary_web_endpoint:           "" => "<computed>"
  secondary_web_host:               "" => "<computed>"
  tags.%:                           "" => "<computed>"[0m
[0m[1mazurerm_virtual_network.vnet: Creating...[0m
  address_space.#:     "" => "1"
  address_space.0:     "" => "10.0.0.0/16"
  location:            "" => "westeurope"
  name:                "" => "vnet"
  resource_group_name: "" => "demo"
  subnet.#:            "" => "<computed>"
  tags.%:              "" => "<computed>"[0m
[0m[1mazurerm_availability_set.avset: Creation complete after 1s (ID: /subscriptions/46c06990-65da-4bc8-86ea-...te/availabilitySets/demojavaiac19avset)[0m[0m
[0m[1mazurerm_public_ip.lbpip: Creation complete after 4s (ID: /subscriptions/46c06990-65da-4bc8-86ea-...twork/publicIPAddresses/demojavaiac-ip)[0m[0m
[0m[1mazurerm_lb.lb: Creating...[0m
  frontend_ip_configuration.#:                       "" => "1"
  frontend_ip_configuration.0.inbound_nat_rules.#:   "" => "<computed>"
  frontend_ip_configuration.0.load_balancer_rules.#: "" => "<computed>"
  frontend_ip_configuration.0.name:                  "" => "LoadBalancerFrontEnd"
  frontend_ip_configuration.0.outbound_rules.#:      "" => "<computed>"
  frontend_ip_configuration.0.private_ip_address:    "" => "<computed>"
  frontend_ip_configuration.0.public_ip_address_id:  "" => "/subscriptions/xxxxxxxxxxxxxxxxxxxxxxxxxx/resourceGroups/demo/providers/Microsoft.Network/publicIPAddresses/demojavaiac-ip"
  frontend_ip_configuration.0.subnet_id:             "" => "<computed>"
  location:                                          "" => "westeurope"
  name:                                              "" => "rglb"
  private_ip_address:                                "" => "<computed>"
  private_ip_addresses.#:                            "" => "<computed>"
  resource_group_name:                               "" => "demo"
  sku:                                               "" => "Basic"
  tags.%:                                            "" => "<computed>"[0m
[0m[1mazurerm_lb.lb: Creation complete after 1s (ID: /subscriptions/46c06990-65da-4bc8-86ea-...s/Microsoft.Network/loadBalancers/rglb)[0m[0m
[0m[1mazurerm_lb_nat_rule.tcp[2]: Creating...[0m
  backend_ip_configuration_id:    "" => "<computed>"
  backend_port:                   "" => "22"
  enable_floating_ip:             "" => "<computed>"
  frontend_ip_configuration_id:   "" => "<computed>"
  frontend_ip_configuration_name: "" => "LoadBalancerFrontEnd"
  frontend_port:                  "" => "50003"
  loadbalancer_id:                "" => "/subscriptions/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx/resourceGroups/demo/providers/Microsoft.Network/loadBalancers/rglb"
  name:                           "" => "SSH-VM-2"
  protocol:                       "" => "tcp"
  resource_group_name:            "" => "demo"[0m
[0m[1mazurerm_lb_backend_address_pool.backend_pool: Creating...[0m
  backend_ip_configurations.#: "" => "<computed>"
  load_balancing_rules.#:      "" => "<computed>"
  loadbalancer_id:             "" => "/subscriptions/xxxxxxxxxxxxxxxxxxxxxxxxxxxxx/resourceGroups/demo/providers/Microsoft.Network/loadBalancers/rglb"
  name:                        "" => "BackendPool1"
  resource_group_name:         "" => "demo"[0m
[0m[1mazurerm_lb_nat_rule.tcp[0]: Creating...[0m
  backend_ip_configuration_id:    "" => "<computed>"
  backend_port:                   "" => "22"
  enable_floating_ip:             "" => "<computed>"
  frontend_ip_configuration_id:   "" => "<computed>"
  frontend_ip_configuration_name: "" => "LoadBalancerFrontEnd"
  frontend_port:                  "" => "50001"
  loadbalancer_id:                "" => "/subscriptions/46c06990-65da-4bc8-86ea-dc3631d5b4c0/resourceGroups/demo/providers/Microsoft.Network/loadBalancers/rglb"
  name:                           "" => "SSH-VM-0"
  protocol:                       "" => "tcp"
  resource_group_name:            "" => "demo"[0m
[0m[1mazurerm_lb_probe.lb_probe: Creating...[0m
  interval_in_seconds:   "" => "5"
  load_balancer_rules.#: "" => "<computed>"
  loadbalancer_id:       "" => "/subscriptions/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx/resourceGroups/demo/providers/Microsoft.Network/loadBalancers/rglb"
  name:                  "" => "tcpProbe"
  number_of_probes:      "" => "3"
  port:                  "" => "8080"
  protocol:              "" => "tcp"
  resource_group_name:   "" => "demo"[0m
[0m[1mazurerm_lb_nat_rule.tcp[1]: Creating...[0m
  backend_ip_configuration_id:    "" => "<computed>"
  backend_port:                   "" => "22"
  enable_floating_ip:             "" => "<computed>"
  frontend_ip_configuration_id:   "" => "<computed>"
  frontend_ip_configuration_name: "" => "LoadBalancerFrontEnd"
  frontend_port:                  "" => "50002"
  loadbalancer_id:                "" => "/subscriptions/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx/resourceGroups/demo/providers/Microsoft.Network/loadBalancers/rglb"
  name:                           "" => "SSH-VM-1"
  protocol:                       "" => "tcp"
  resource_group_name:            "" => "demo"[0m
[0m[1mazurerm_lb_probe.lb_probe: Creation complete after 1s (ID: /subscriptions/46c06990-65da-4bc8-86ea-...ork/loadBalancers/rglb/probes/tcpProbe)[0m[0m
[0m[1mazurerm_lb_backend_address_pool.backend_pool: Creation complete after 2s (ID: /subscriptions/46c06990-65da-4bc8-86ea-.../rglb/backendAddressPools/BackendPool1)[0m[0m
[0m[1mazurerm_lb_rule.lb_rule: Creating...[0m
  backend_address_pool_id:        "" => "/subscriptions/46c06990-65da-4bc8-86ea-dc3631d5b4c0/resourceGroups/demo/providers/Microsoft.Network/loadBalancers/rglb/backendAddressPools/BackendPool1"
  backend_port:                   "" => "8080"
  enable_floating_ip:             "" => "false"
  frontend_ip_configuration_id:   "" => "<computed>"
  frontend_ip_configuration_name: "" => "LoadBalancerFrontEnd"
  frontend_port:                  "" => "80"
  idle_timeout_in_minutes:        "" => "5"
  load_distribution:              "" => "<computed>"
  loadbalancer_id:                "" => "/subscriptions/46c06990-65da-4bc8-86ea-dc3631d5b4c0/resourceGroups/demo/providers/Microsoft.Network/loadBalancers/rglb"
  name:                           "" => "LBRule"
  probe_id:                       "" => "/subscriptions/46c06990-65da-4bc8-86ea-dc3631d5b4c0/resourceGroups/demo/providers/Microsoft.Network/loadBalancers/rglb/probes/tcpProbe"
  protocol:                       "" => "tcp"
  resource_group_name:            "" => "demo"[0m
[0m[1mazurerm_lb_nat_rule.tcp[2]: Creation complete after 3s (ID: /subscriptions/46c06990-65da-4bc8-86ea-...alancers/rglb/inboundNatRules/SSH-VM-2)[0m[0m
[0m[1mazurerm_lb_nat_rule.tcp[0]: Creation complete after 4s (ID: /subscriptions/46c06990-65da-4bc8-86ea-...alancers/rglb/inboundNatRules/SSH-VM-0)[0m[0m
[0m[1mazurerm_lb_nat_rule.tcp[1]: Creation complete after 4s (ID: /subscriptions/46c06990-65da-4bc8-86ea-...alancers/rglb/inboundNatRules/SSH-VM-1)[0m[0m
[0m[1mazurerm_lb_rule.lb_rule: Creation complete after 3s (ID: /subscriptions/46c06990-65da-4bc8-86ea-...lancers/rglb/loadBalancingRules/LBRule)[0m[0m
[0m[1mazurerm_storage_account.stor: Still creating... (10s elapsed)[0m[0m
[0m[1mazurerm_virtual_network.vnet: Still creating... (10s elapsed)[0m[0m
[0m[1mazurerm_virtual_network.vnet: Creation complete after 12s (ID: /subscriptions/46c06990-65da-4bc8-86ea-...Microsoft.Network/virtualNetworks/vnet)[0m[0m
[0m[1mazurerm_subnet.subnet: Creating...[0m
  address_prefix:       "" => "10.0.10.0/24"
  ip_configurations.#:  "" => "<computed>"
  name:                 "" => "rgsubnet"
  resource_group_name:  "" => "demo"
  virtual_network_name: "" => "vnet"[0m
[0m[1mazurerm_storage_account.stor: Creation complete after 19s (ID: /subscriptions/46c06990-65da-4bc8-86ea-...rage/storageAccounts/demojavaiac19stor)[0m[0m
[0m[1mazurerm_subnet.subnet: Still creating... (10s elapsed)[0m[0m
[0m[1mazurerm_subnet.subnet: Creation complete after 10s (ID: /subscriptions/46c06990-65da-4bc8-86ea-.../virtualNetworks/vnet/subnets/rgsubnet)[0m[0m
[0m[1mazurerm_network_interface.nic[0]: Creating...[0m
  applied_dns_servers.#:                                               "" => "<computed>"
  dns_servers.#:                                                       "" => "<computed>"
  enable_accelerated_networking:                                       "" => "false"
  enable_ip_forwarding:                                                "" => "false"
  internal_dns_name_label:                                             "" => "<computed>"
  internal_fqdn:                                                       "" => "<computed>"
  ip_configuration.#:                                                  "" => "1"
  ip_configuration.0.application_gateway_backend_address_pools_ids.#:  "" => "<computed>"
  ip_configuration.0.application_security_group_ids.#:                 "" => "<computed>"
  ip_configuration.0.load_balancer_backend_address_pools_ids.#:        "" => "1"
  ip_configuration.0.load_balancer_backend_address_pools_ids.40986060: "" => "/subscriptions/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx/resourceGroups/demo/providers/Microsoft.Network/loadBalancers/rglb/backendAddressPools/BackendPool1"
  ip_configuration.0.load_balancer_inbound_nat_rules_ids.#:            "" => "1"
  ip_configuration.0.load_balancer_inbound_nat_rules_ids.719561066:    "" => "/subscriptions/xxxxxxxxxxxxxxxxxxxxxxxxxx/resourceGroups/demo/providers/Microsoft.Network/loadBalancers/rglb/inboundNatRules/SSH-VM-0"
  ip_configuration.0.name:                                             "" => "ipconfig0"
  ip_configuration.0.primary:                                          "" => "<computed>"
  ip_configuration.0.private_ip_address_allocation:                    "" => "dynamic"
  ip_configuration.0.private_ip_address_version:                       "" => "IPv4"
  ip_configuration.0.subnet_id:                                        "" => "/subscriptions/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx/resourceGroups/demo/providers/Microsoft.Network/virtualNetworks/vnet/subnets/rgsubnet"
  location:                                                            "" => "westeurope"
  mac_address:                                                         "" => "<computed>"
  name:                                                                "" => "nic0"
  private_ip_address:                                                  "" => "<computed>"
  private_ip_addresses.#:                                              "" => "<computed>"
  resource_group_name:                                                 "" => "demo"
  tags.%:                                                              "" => "<computed>"
  virtual_machine_id:                                                  "" => "<computed>"[0m
[0m[1mazurerm_network_interface.nic[1]: Creating...[0m
  applied_dns_servers.#:                                               "" => "<computed>"
  dns_servers.#:                                                       "" => "<computed>"
  enable_accelerated_networking:                                       "" => "false"
  enable_ip_forwarding:                                                "" => "false"
  internal_dns_name_label:                                             "" => "<computed>"
  internal_fqdn:                                                       "" => "<computed>"
  ip_configuration.#:                                                  "" => "1"
  ip_configuration.0.application_gateway_backend_address_pools_ids.#:  "" => "<computed>"
  ip_configuration.0.application_security_group_ids.#:                 "" => "<computed>"
  ip_configuration.0.load_balancer_backend_address_pools_ids.#:        "" => "1"
  ip_configuration.0.load_balancer_backend_address_pools_ids.40986060: "" => "/subscriptions/46c06990-65da-4bc8-86ea-dc3631d5b4c0/resourceGroups/demo/providers/Microsoft.Network/loadBalancers/rglb/backendAddressPools/BackendPool1"
  ip_configuration.0.load_balancer_inbound_nat_rules_ids.#:            "" => "1"
  ip_configuration.0.load_balancer_inbound_nat_rules_ids.1575260668:   "" => "/subscriptions/46c06990-65da-4bc8-86ea-dc3631d5b4c0/resourceGroups/demo/providers/Microsoft.Network/loadBalancers/rglb/inboundNatRules/SSH-VM-1"
  ip_configuration.0.name:                                             "" => "ipconfig1"
  ip_configuration.0.primary:                                          "" => "<computed>"
  ip_configuration.0.private_ip_address_allocation:                    "" => "dynamic"
  ip_configuration.0.private_ip_address_version:                       "" => "IPv4"
  ip_configuration.0.subnet_id:                                        "" => "/subscriptions/46c06990-65da-4bc8-86ea-dc3631d5b4c0/resourceGroups/demo/providers/Microsoft.Network/virtualNetworks/vnet/subnets/rgsubnet"
  location:                                                            "" => "westeurope"
  mac_address:                                                         "" => "<computed>"
  name:                                                                "" => "nic1"
  private_ip_address:                                                  "" => "<computed>"
  private_ip_addresses.#:                                              "" => "<computed>"
  resource_group_name:                                                 "" => "demo"
  tags.%:                                                              "" => "<computed>"
  virtual_machine_id:                                                  "" => "<computed>"[0m
[0m[1mazurerm_network_interface.nic[2]: Creating...[0m
  applied_dns_servers.#:                                               "" => "<computed>"
  dns_servers.#:                                                       "" => "<computed>"
  enable_accelerated_networking:                                       "" => "false"
  enable_ip_forwarding:                                                "" => "false"
  internal_dns_name_label:                                             "" => "<computed>"
  internal_fqdn:                                                       "" => "<computed>"
  ip_configuration.#:                                                  "" => "1"
  ip_configuration.0.application_gateway_backend_address_pools_ids.#:  "" => "<computed>"
  ip_configuration.0.application_security_group_ids.#:                 "" => "<computed>"
  ip_configuration.0.load_balancer_backend_address_pools_ids.#:        "" => "1"
  ip_configuration.0.load_balancer_backend_address_pools_ids.40986060: "" => "/subscriptions/46c06990-65da-4bc8-86ea-dc3631d5b4c0/resourceGroups/demo/providers/Microsoft.Network/loadBalancers/rglb/backendAddressPools/BackendPool1"
  ip_configuration.0.load_balancer_inbound_nat_rules_ids.#:            "" => "1"
  ip_configuration.0.load_balancer_inbound_nat_rules_ids.3303915590:   "" => "/subscriptions/46c06990-65da-4bc8-86ea-dc3631d5b4c0/resourceGroups/demo/providers/Microsoft.Network/loadBalancers/rglb/inboundNatRules/SSH-VM-2"
  ip_configuration.0.name:                                             "" => "ipconfig2"
  ip_configuration.0.primary:                                          "" => "<computed>"
  ip_configuration.0.private_ip_address_allocation:                    "" => "dynamic"
  ip_configuration.0.private_ip_address_version:                       "" => "IPv4"
  ip_configuration.0.subnet_id:                                        "" => "/subscriptions/46c06990-65da-4bc8-86ea-dc3631d5b4c0/resourceGroups/demo/providers/Microsoft.Network/virtualNetworks/vnet/subnets/rgsubnet"
  location:                                                            "" => "westeurope"
  mac_address:                                                         "" => "<computed>"
  name:                                                                "" => "nic2"
  private_ip_address:                                                  "" => "<computed>"
  private_ip_addresses.#:                                              "" => "<computed>"
  resource_group_name:                                                 "" => "demo"
  tags.%:                                                              "" => "<computed>"
  virtual_machine_id:                                                  "" => "<computed>"[0m
[0m[1mazurerm_network_interface.nic[2]: Creation complete after 1s (ID: /subscriptions/46c06990-65da-4bc8-86ea-...crosoft.Network/networkInterfaces/nic2)[0m[0m
[0m[1mazurerm_network_interface.nic[0]: Creation complete after 2s (ID: /subscriptions/46c06990-65da-4bc8-86ea-...crosoft.Network/networkInterfaces/nic0)[0m[0m
[0m[1mazurerm_network_interface.nic[1]: Creation complete after 3s (ID: /subscriptions/46c06990-65da-4bc8-86ea-...crosoft.Network/networkInterfaces/nic1)[0m[0m
[0m[1mazurerm_virtual_machine.vm[0]: Creating...[0m
  availability_set_id:                                              "" => "/subscriptions/46c06990-65da-4bc8-86ea-dc3631d5b4c0/resourcegroups/demo/providers/microsoft.compute/availabilitysets/demojavaiac19avset"
  delete_data_disks_on_termination:                                 "" => "false"
  delete_os_disk_on_termination:                                    "" => "false"
  identity.#:                                                       "" => "<computed>"
  location:                                                         "" => "westeurope"
  name:                                                             "" => "vm0"
  network_interface_ids.#:                                          "" => "1"
  network_interface_ids.0:                                          "" => "/subscriptions/46c06990-65da-4bc8-86ea-dc3631d5b4c0/resourceGroups/demo/providers/Microsoft.Network/networkInterfaces/nic0"
  os_profile.#:                                                     "" => "1"
  os_profile.1770182618.admin_password:                             "<sensitive>" => "<sensitive>"
  os_profile.1770182618.admin_username:                             "" => "azureuser"
  os_profile.1770182618.computer_name:                              "" => "myvm"
  os_profile.1770182618.custom_data:                                "" => "<computed>"
  os_profile_linux_config.#:                                        "" => "1"
  os_profile_linux_config.69840937.disable_password_authentication: "" => "true"
  os_profile_linux_config.69840937.ssh_keys.#:                      "" => "1"
  os_profile_linux_config.69840937.ssh_keys.0.key_data:             "" => "ssh-rsa xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
  os_profile_linux_config.69840937.ssh_keys.0.path:                 "" => "/home/azureuser/.ssh/authorized_keys"
  resource_group_name:                                              "" => "demo"
  storage_data_disk.#:                                              "" => "<computed>"
  storage_image_reference.#:                                        "" => "1"
  storage_image_reference.2250550379.id:                            "" => ""
  storage_image_reference.2250550379.offer:                         "" => "RHEL"
  storage_image_reference.2250550379.publisher:                     "" => "RedHat"
  storage_image_reference.2250550379.sku:                           "" => "7.3"
  storage_image_reference.2250550379.version:                       "" => "latest"
  storage_os_disk.#:                                                "" => "1"
  storage_os_disk.0.caching:                                        "" => "<computed>"
  storage_os_disk.0.create_option:                                  "" => "FromImage"
  storage_os_disk.0.disk_size_gb:                                   "" => "<computed>"
  storage_os_disk.0.managed_disk_id:                                "" => "<computed>"
  storage_os_disk.0.managed_disk_type:                              "" => "<computed>"
  storage_os_disk.0.name:                                           "" => "osdisk0"
  storage_os_disk.0.write_accelerator_enabled:                      "" => "false"
  tags.%:                                                           "" => "1"
  tags.environment:                                                 "" => "Terraform Demo"
  vm_size:                                                          "" => "Standard_DS1_v2"[0m
[0m[1mazurerm_virtual_machine.vm[1]: Creating...[0m
  availability_set_id:                                              "" => "/subscriptions/46c06990-65da-4bc8-86ea-dc3631d5b4c0/resourcegroups/demo/providers/microsoft.compute/availabilitysets/demojavaiac19avset"
  delete_data_disks_on_termination:                                 "" => "false"
  delete_os_disk_on_termination:                                    "" => "false"
  identity.#:                                                       "" => "<computed>"
  location:                                                         "" => "westeurope"
  name:                                                             "" => "vm1"
  network_interface_ids.#:                                          "" => "1"
  network_interface_ids.0:                                          "" => "/subscriptions/46c06990-65da-4bc8-86ea-dc3631d5b4c0/resourceGroups/demo/providers/Microsoft.Network/networkInterfaces/nic1"
  os_profile.#:                                                     "" => "1"
  os_profile.1770182618.admin_password:                             "<sensitive>" => "<sensitive>"
  os_profile.1770182618.admin_username:                             "" => "azureuser"
  os_profile.1770182618.computer_name:                              "" => "myvm"
  os_profile.1770182618.custom_data:                                "" => "<computed>"
  os_profile_linux_config.#:                                        "" => "1"
  os_profile_linux_config.69840937.disable_password_authentication: "" => "true"
  os_profile_linux_config.69840937.ssh_keys.#:                      "" => "1"
  os_profile_linux_config.69840937.ssh_keys.0.key_data:             "" => "ssh-rsa xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
  os_profile_linux_config.69840937.ssh_keys.0.path:                 "" => "/home/azureuser/.ssh/authorized_keys"
  resource_group_name:                                              "" => "demo"
  storage_data_disk.#:                                              "" => "<computed>"
  storage_image_reference.#:                                        "" => "1"
  storage_image_reference.2250550379.id:                            "" => ""
  storage_image_reference.2250550379.offer:                         "" => "RHEL"
  storage_image_reference.2250550379.publisher:                     "" => "RedHat"
  storage_image_reference.2250550379.sku:                           "" => "7.3"
  storage_image_reference.2250550379.version:                       "" => "latest"
  storage_os_disk.#:                                                "" => "1"
  storage_os_disk.0.caching:                                        "" => "<computed>"
  storage_os_disk.0.create_option:                                  "" => "FromImage"
  storage_os_disk.0.disk_size_gb:                                   "" => "<computed>"
  storage_os_disk.0.managed_disk_id:                                "" => "<computed>"
  storage_os_disk.0.managed_disk_type:                              "" => "<computed>"
  storage_os_disk.0.name:                                           "" => "osdisk1"
  storage_os_disk.0.write_accelerator_enabled:                      "" => "false"
  tags.%:                                                           "" => "1"
  tags.environment:                                                 "" => "Terraform Demo"
  vm_size:                                                          "" => "Standard_DS1_v2"[0m
[0m[1mazurerm_virtual_machine.vm[2]: Creating...[0m
  availability_set_id:                                              "" => "/subscriptions/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx/resourcegroups/demo/providers/microsoft.compute/availabilitysets/demojavaiac19avset"
  delete_data_disks_on_termination:                                 "" => "false"
  delete_os_disk_on_termination:                                    "" => "false"
  identity.#:                                                       "" => "<computed>"
  location:                                                         "" => "westeurope"
  name:                                                             "" => "vm2"
  network_interface_ids.#:                                          "" => "1"
  network_interface_ids.0:                                          "" => "/subscriptions/xxxxxxxxxxxxxxxxx/resourceGroups/demo/providers/Microsoft.Network/networkInterfaces/nic2"
  os_profile.#:                                                     "" => "1"
  os_profile.1770182618.admin_password:                             "<sensitive>" => "<sensitive>"
  os_profile.1770182618.admin_username:                             "" => "azureuser"
  os_profile.1770182618.computer_name:                              "" => "myvm"
  os_profile.1770182618.custom_data:                                "" => "<computed>"
  os_profile_linux_config.#:                                        "" => "1"
  os_profile_linux_config.69840937.disable_password_authentication: "" => "true"
  os_profile_linux_config.69840937.ssh_keys.#:                      "" => "1"
  os_profile_linux_config.69840937.ssh_keys.0.key_data:             "" => "ssh-rsa xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
  os_profile_linux_config.69840937.ssh_keys.0.path:                 "" => "/home/azureuser/.ssh/authorized_keys"
  resource_group_name:                                              "" => "demo"
  storage_data_disk.#:                                              "" => "<computed>"
  storage_image_reference.#:                                        "" => "1"
  storage_image_reference.2250550379.id:                            "" => ""
  storage_image_reference.2250550379.offer:                         "" => "RHEL"
  storage_image_reference.2250550379.publisher:                     "" => "RedHat"
  storage_image_reference.2250550379.sku:                           "" => "7.3"
  storage_image_reference.2250550379.version:                       "" => "latest"
  storage_os_disk.#:                                                "" => "1"
  storage_os_disk.0.caching:                                        "" => "<computed>"
  storage_os_disk.0.create_option:                                  "" => "FromImage"
  storage_os_disk.0.disk_size_gb:                                   "" => "<computed>"
  storage_os_disk.0.managed_disk_id:                                "" => "<computed>"
  storage_os_disk.0.managed_disk_type:                              "" => "<computed>"
  storage_os_disk.0.name:                                           "" => "osdisk2"
  storage_os_disk.0.write_accelerator_enabled:                      "" => "false"
  tags.%:                                                           "" => "1"
  tags.environment:                                                 "" => "Terraform Demo"
  vm_size:                                                          "" => "Standard_DS1_v2"[0m
[0m[1mazurerm_virtual_machine.vm.0: Still creating... (10s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.1: Still creating... (10s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.2: Still creating... (10s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.0: Still creating... (20s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.2: Still creating... (20s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.1: Still creating... (20s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.0: Still creating... (30s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.2: Still creating... (30s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.1: Still creating... (30s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.0: Still creating... (40s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.2: Still creating... (40s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.1: Still creating... (40s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.0: Still creating... (50s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.2: Still creating... (50s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.1: Still creating... (50s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.0: Still creating... (1m0s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.2: Still creating... (1m0s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.1: Still creating... (1m0s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.0: Still creating... (1m10s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.2: Still creating... (1m10s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.1: Still creating... (1m10s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.0: Still creating... (1m20s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.2: Still creating... (1m20s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.1: Still creating... (1m20s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.0: Still creating... (1m30s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.2: Still creating... (1m30s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.1: Still creating... (1m30s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.0: Still creating... (1m40s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.2: Still creating... (1m40s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.1: Still creating... (1m40s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.0: Still creating... (1m50s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.2: Still creating... (1m50s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.1: Still creating... (1m50s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.0: Still creating... (2m0s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.2: Still creating... (2m0s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.1: Still creating... (2m0s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.0: Still creating... (2m10s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.2: Still creating... (2m10s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.1: Still creating... (2m10s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.0: Still creating... (2m20s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.2: Still creating... (2m20s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.1: Still creating... (2m20s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.0: Still creating... (2m30s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.2: Still creating... (2m30s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm.1: Still creating... (2m30s elapsed)[0m[0m
[0m[1mazurerm_virtual_machine.vm[2]: Creation complete after 2m32s (ID: /subscriptions/46c06990-65da-4bc8-86ea-.../Microsoft.Compute/virtualMachines/vm2)[0m[0m
[0m[1mazurerm_virtual_machine.vm[0]: Creation complete after 2m32s (ID: /subscriptions/46c06990-65da-4bc8-86ea-.../Microsoft.Compute/virtualMachines/vm0)[0m[0m
[0m[1mazurerm_virtual_machine.vm[1]: Creation complete after 2m32s (ID: /subscriptions/46c06990-65da-4bc8-86ea-.../Microsoft.Compute/virtualMachines/vm1)[0m[0m
[0m[1m[32m
Apply complete! Resources: 19 added, 0 changed, 0 destroyed.[0m
[0m[1m[32m
Outputs:

vm_dns = http://demojavaiac.westeurope.cloudapp.azure.com
vm_ip = demojavaiac.westeurope.cloudapp.azure.com[0m
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage

[Pipeline] { (Results)

[Pipeline] ansiblePlaybook

[test] $ ansible-playbook /root/.jenkins/workspace/test/ansible/Java-app.yml -i /root/.jenkins/workspace/test/ansible/inventory

PLAY [all] *********************************************************************

TASK [Gathering Facts] *********************************************************
[0;32mok: [host1][0m
[0;32mok: [host3][0m
[0;32mok: [host2][0m

TASK [selinux : Download EPEL Repo - Centos/RHEL 6] ****************************
[0;36mskipping: [host1][0m
[0;36mskipping: [host2][0m
[0;36mskipping: [host3][0m

TASK [selinux : Install EPEL Repo - Centos/RHEL 6] *****************************
[0;36mskipping: [host1][0m
[0;36mskipping: [host2][0m
[0;36mskipping: [host3][0m

TASK [selinux : Download EPEL Repo - Centos/RHEL 7] ****************************
[0;33mchanged: [host1][0m
[0;33mchanged: [host3][0m
[0;33mchanged: [host2][0m

TASK [selinux : Install EPEL Repo - Centos/RHEL 7] *****************************
[1;35m [WARNING]: Consider using the yum, dnf or zypper module rather than running[0m
[1;35m'rpm'.  If you need to use command because yum, dnf or zypper is insufficient[0m
[1;35myou can add 'warn: false' to this command task or set 'command_warnings=False'[0m
[1;35min ansible.cfg to get rid of this message.[0m
[1;35m[0m
[0;33mchanged: [host1][0m
[0;33mchanged: [host3][0m
[0;33mchanged: [host2][0m

TASK [selinux : Install libselinux-python] *************************************
[0;32mok: [host1][0m
[0;32mok: [host3][0m
[0;32mok: [host2][0m

TASK [tomcat : Install Java 1.7] ***********************************************
[0;33mchanged: [host1][0m
[0;33mchanged: [host3][0m
[0;33mchanged: [host2][0m

TASK [tomcat : add group "tomcat"] *********************************************
[0;33mchanged: [host2][0m
[0;33mchanged: [host3][0m
[0;33mchanged: [host1][0m

TASK [tomcat : add user "tomcat"] **********************************************
[0;33mchanged: [host1][0m
[0;33mchanged: [host3][0m
[0;33mchanged: [host2][0m

TASK [tomcat : Download Tomcat] ************************************************
[0;33mchanged: [host2][0m
[0;33mchanged: [host1][0m
[0;33mchanged: [host3][0m

TASK [tomcat : Extract archive] ************************************************
[1;35m [WARNING]: Consider using the unarchive module rather than running 'tar'.  If[0m
[1;35myou need to use command because unarchive is insufficient you can add 'warn:[0m
[1;35mfalse' to this command task or set 'command_warnings=False' in ansible.cfg to[0m
[1;35mget rid of this message.[0m
[1;35m[0m
[0;33mchanged: [host3][0m
[0;33mchanged: [host2][0m
[0;33mchanged: [host1][0m

TASK [tomcat : Symlink install directory] **************************************
[0;33mchanged: [host3][0m
[0;33mchanged: [host1][0m
[0;33mchanged: [host2][0m

TASK [tomcat : Change ownership of Tomcat installation] ************************
[0;33mchanged: [host1][0m
[0;33mchanged: [host2][0m
[0;33mchanged: [host3][0m

TASK [tomcat : Configure Tomcat server] ****************************************
[0;33mchanged: [host1][0m
[0;33mchanged: [host2][0m
[0;33mchanged: [host3][0m

TASK [tomcat : Configure Tomcat users] *****************************************
[0;33mchanged: [host1][0m
[0;33mchanged: [host2][0m
[0;33mchanged: [host3][0m

TASK [tomcat : Install Tomcat init script] *************************************
[0;33mchanged: [host1][0m
[0;33mchanged: [host2][0m
[0;33mchanged: [host3][0m

TASK [tomcat : Start Tomcat] ***************************************************
[1;35m [WARNING]: The service (tomcat) is actually an init script but the system is[0m
[1;35mmanaged by systemd[0m
[1;35m[0m
[0;33mchanged: [host2][0m
[0;33mchanged: [host3][0m
[0;33mchanged: [host1][0m

TASK [tomcat : deploy iptables rules] ******************************************
[0;36mskipping: [host1][0m
[0;36mskipping: [host2][0m
[0;36mskipping: [host3][0m

TASK [tomcat : insert firewalld rule for tomcat http port] *********************
[0;33mchanged: [host3][0m
[0;33mchanged: [host2][0m
[0;33mchanged: [host1][0m

TASK [tomcat : insert firewalld rule for tomcat https port] ********************
[0;33mchanged: [host1][0m
[0;33mchanged: [host3][0m
[0;33mchanged: [host2][0m
TASK [tomcat : wait for tomcat to start] ***************************************
[0;32mok: [host1][0m
[0;32mok: [host2][0m
[0;32mok: [host3][0m

TASK [tomcat : unDeploy sample app] ********************************************
[0;32mok: [host1][0m
[0;32mok: [host2][0m
[0;32mok: [host3][0m

TASK [tomcat : wait for tomcat to undeploy the app] ****************************
[0;32mok: [host1][0m
[0;32mok: [host3][0m
[0;32mok: [host2][0m

TASK [tomcat : Deploy sample app] **********************************************
[0;33mchanged: [host1][0m
[0;33mchanged: [host2][0m
[0;33mchanged: [host3][0m

TASK [tomcat : Start Tomcat] ***************************************************
[0;32mok: [host2][0m
[0;32mok: [host3][0m
[0;32mok: [host1][0m

RUNNING HANDLER [tomcat : restart tomcat] **************************************
[0;33mchanged: [host1][0m
[0;33mchanged: [host2][0m
[0;33mchanged: [host3][0m

PLAY RECAP *********************************************************************
[0;33mhost1[0m                      : [0;32mok=23  [0m [0;33mchanged=17  [0m unreachable=0    failed=0   
[0;33mhost2[0m                      : [0;32mok=23  [0m [0;33mchanged=17  [0m unreachable=0    failed=0   
[0;33mhost3[0m                      : [0;32mok=23  [0m [0;33mchanged=17  [0m unreachable=0    failed=0 

[Pipeline] }
[Pipeline] // stage
[Pipeline] }
[Pipeline] // node
[Pipeline] End of Pipeline
Finished: SUCCESS
"""
