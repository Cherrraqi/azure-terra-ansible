#!/bin/bash
echo "************* execute terraform apply"
## execute terrafotm build and sendout to packer-build-output
export ARM_CLIENT_ID=$1
export ARM_CLIENT_SECRET=$2
export ARM_SUBSCRIPTION_ID=$3
export ARM_TENANT_ID=$4
#terraform apply -auto-approve -var ssh_key=$6 
terraform apply

export vmss_ip=$(terraform output vm_ip)
echo "host1 ansible_ssh_port=50001 ansible_ssh_host=$vmss_ip" > inventory
echo "host2 ansible_ssh_port=50002 ansible_ssh_host=$vmss_ip" >> inventory
echo "host3 ansible_port=50003 ansible_ssh_host=$vmss_ip" >> inventory

cat inventory
